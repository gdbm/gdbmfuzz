#include <stdio.h>
#include <sys/syscall.h>
#include <stdint.h>
#include <time.h>
#include <sys/stat.h>
#include <gdbmtool.h>

static char dbname[] = "a.db";

struct instream_string
{
  struct instream base;
  char *string;
  size_t length;
  size_t pos;
};

static ssize_t
instream_string_read (instream_t istr, char *buf, size_t size)
{
  struct instream_string *str = (struct instream_string *)istr;
  size_t n = str->length - str->pos;
  if (size > n)
    size = n;
  memcpy (buf, str->string + str->pos, n);
  str->pos += n;
  return n;
}

static void
instream_string_close (instream_t istr)
{
  struct instream_string *str = (struct instream_string *)istr;
  str->pos = 0;
}

static int
instream_string_eq (instream_t a, instream_t b)
{
  return 0;
}

static instream_t
instream_string_create (char const *input)
{
  struct instream_string *istr;
  size_t len;
  int nl;
  
  istr = emalloc (sizeof (*istr));
  istr->base.in_name = "string";
  istr->base.in_inter = 0;
  istr->base.in_read = instream_string_read;
  istr->base.in_close = instream_string_close;
  istr->base.in_eq = instream_string_eq;
  istr->base.in_history_size = NULL;
  istr->base.in_history_get = NULL;
  len = strlen (input);
  while (len > 0 && (input[len-1] == ' ' || input[len-1] == '\t'))
    --len;

  nl = len > 0 && input[len-1] != '\n';
  istr->string = emalloc (len + nl + 1);
  memcpy (istr->string, input, len);
  if (nl)
    istr->string[len++] = '\n';
  istr->string[len] = 0;
  istr->length = len;
  istr->pos = 0;

  return (instream_t) istr;
}

static instream_t input;

static void
fuzz_init (void)
{
  char *p;

  if (input)
    return;
  
  set_progname ("gdbmfuzz");

  if ((p = getenv ("GDBMFUZZ_INPUT")) != NULL)
    {
      input = instream_string_create (p);
      if (!input)
	exit (1);
    }
  else
    {
      terror ("GDBMFUZZ_INPUT must be set");
      exit (1);
    }
}

int
LLVMFuzzerTestOneInput (const uint8_t *data, size_t size)
{
  int fd;
  GDBM_FILE db;
  
  fd = syscall (SYS_memfd_create, dbname, 0);
  if (fd == -1)
    {
      perror ("memfd_create");
      exit (1);
    }

  if (write (fd, data, size) < size)
    {
      close (fd);
      perror ("write");
      exit (1);
    }

  if (lseek (fd, 0, SEEK_SET) != 0)
    {
      close (fd);
      perror ("write");
      exit (1);
    }

  fuzz_init ();
  
  variable_set ("filename", VART_STRING, dbname);
  variable_set ("fd", VART_INT, &fd);

  return gdbmshell (input);
}
