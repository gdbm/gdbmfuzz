CC=clang
CFLAGS=-g3 -fsanitize=address
TESTDIR=tests
INPUT=fuzz.in

check: gdbmfuzz runtest.sh
	sh ./runtest.sh $(TESTDIR) $(INPUT)

gdbmfuzz: gdbmfuzz.c gdbm/src/libgdbm.la
	$(CC) ${CFLAGS},fuzzer -Igdbm -Igdbm/src gdbmfuzz.c -o gdbmfuzz -Lgdbm/src -Lgdbm/src/.libs -lgdbmapp -lgdbm

gdbm/src/libgdbm.la: gdbm/Makefile
	make -C gdbm

gdbm/Makefile: gdbm/configure
	cd gdbm && ./configure --disable-shared --enable-debug --disable-nls CFLAGS="$(CFLAGS),fuzzer-no-link -O2 -g3" CC=$(CC)

gdbm/configure: gdbm/configure.ac
	cd gdbm && ./bootstrap

gdbm/configure.ac: Makefile
	git submodule init
	git submodule update
