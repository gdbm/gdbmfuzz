#!/bin/sh

set -e

PATH=$(pwd):$(pwd)/gdbm/src:$PATH

wd=${1:?}
GDBMFUZZ_INPUT=$(cat ${2:?})
export GDBMFUZZ_INPUT

populate() {
    for format in standard numsync; do
	gdbmtool << EOF
set format=$format
open empty_$format
close

open one_$format
store key1 value1
close

set blocksize=512
open empty_b512_$format
close

set cachesize=512
open empty_b512_c512_$format
close

open one_b512_c512_$format
store key1 value1
close

open ten_b512_c512_$format
store key1 value1
store key2 value2
store key3 value3
store key4 value4
store key5 value5
store key6 value6
store key7 value7
store key8 value8
store key9 value9
store key10 value10

open nine_b512_c512_$format
store key1 value1
store key2 value2
store key3 value3
store key4 value4
store key5 value5
store key6 value6
store key7 value7
store key8 value8
store key9 value9
store key10 value10
delete key1
close

open one_b512_c512_ku_cs_$format
define key { uint k }
define content { string s }
store 1 value1
close

open one_b512_c512_ku_cu_$format
define key { uint k }
define content { uint v }
store 1 1
define key { string k }
store key1 1
define key { uint k }
define content { uint v[2] }
store 1 { { 1 , 2 } }
list
close

open one_b512_c512_ku_cusz_$format
define key { uint k }
define content { uint v, stringz s }
store 1 { 1 , value1 }
list
close
quit
EOF
	done
}

if [ -d $wd ]; then
    cd $wd    
else
    mkdir $wd
    cd $wd
    populate
fi

gdbmfuzz -close_fd_mask=3 .
